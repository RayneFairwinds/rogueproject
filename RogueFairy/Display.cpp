#include "stdafx.h"

int Display::update() {
	refresh();

	updateMap();
	updateConsole();
	updateChat();

	input = getch();

	if (input == KEY_F(1))
		return 0;

	return 1;
}
int Display::updateMap() {

	wrefresh(map_win);
	int x, y;
	x = getmaxx(map_win);
	y = getmaxy(map_win);
	
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++) {
			wmove(map_win, i, j);
			waddch(map_win, city.activeMap[i][j]->symbol);
		}
	}
	//*/
	return 1;
}

int Display::updateChat() {
	
	wrefresh(chat_win);

	return 1;
}

int Display::updateConsole() {
	
	wrefresh(con_win);

	return 1;
}