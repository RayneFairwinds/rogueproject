#pragma once

struct Tile {
	char symbol = '.';
	bool passable = true;
	bool blockLOS = false;
};

class CityGen {
	
public:
	const int aMapH = 100;
	const int aMapW = 100;
	Tile activeMap[100][100];
	Tile wall;

	
	CityGen() {
		wall.symbol = '#';
		wall.passable = false;
		wall.blockLOS = true;

		/*for (int i = 0; i < aMapH; i++)
			for (int j = 0; j < aMapW; j++)
				activeMap[i][j];
				*/
	}
};