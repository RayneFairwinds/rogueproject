#pragma once
#include <curses.h>
#include "CityGen.h"

class Display {
protected:
	int input;

	const int mapSizeY = 2 / 3; //map will take 2/3 of the screen
	const int mapSizeX = 2 / 3;

	int screenY = 0;
	int screenX = 0;
	int sMapY = 0;
	int sMapX = 0;
	int sChatY = 0;
	int sChatX = 0;
	int sConsoleY = 0;
	int sConsoleX = 0;
	WINDOW *map_win;
	WINDOW *chat_win;
	WINDOW *con_win;

	CityGen city;

public:
	Display() {
		initscr();
		raw();
		noecho();
		curs_set(0);
		keypad(stdscr, TRUE);
		start_color();

		getmaxyx(stdscr, screenY, screenX);
		
		sMapY = LINES /3;
		sMapX = COLS /3;

		map_win = newwin(LINES-sMapY, COLS - sMapX, 0, 0);
		box(map_win, 0, 0);

		sChatX = COLS * 2/ 3;
		chat_win = newwin(sMapY*2, sMapX-1, 0, COLS - sMapX);
		box(chat_win, 0, 0);
		
		con_win = newwin(sMapY, COLS-1, sMapY*2, 0);
		box(con_win, 0, 0);

		mvwprintw(map_win, 1, 1,"My name is Inigo Montoya");
		mvwprintw(chat_win, 1, 1, "You killed my father");
		mvwprintw(con_win, 1, 1, "prepare to die!");
	};

	~Display() {
		endwin();
	}

	int updateMap();
	int updateChat();
	int updateConsole();
	int update();
};